***Settings***
Documentation       Representação a página equipamentos com seus elementos e ações


***Variables***
${CUSTOMERS_FORM}       css:a[href$=register]
${LABEL_NAME}           css:label[for=equipo-name]
${LABEL_VALOR}          css:label[for=daily_price]

***Keywords***
Register New Equipos
    [Arguments]     ${name}     ${valor}

    Input Text      id:equipo-name              ${name}
    Input Text      id:daily_price              ${valor}

    Click Element   xpath://button[text()='CADASTRAR']