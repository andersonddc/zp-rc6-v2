***Settings***
Documentation       Cadastro de equipamentos

Resource    ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

***Test Cases***
Novo Equipamento
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...         piano         100
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:   Equipo cadastrado com sucesso!

Nome Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    ${EMPTY}        120         Nome do equipo é obrigatório      

Valor Obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    bateria         ${EMPTY}        Diária do equipo é obrigatória

Equipamento Duplicado
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...         violão    150
    Mas esse equipamento já existe no sistema
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação de erro:   Erro na criação de um equipo

***Keywords***
Validação de Campos
    [Arguments]     ${nome}     ${valor}    ${saida}          

    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...         ${nome}    ${valor}    
    Quando faço a inclusão desse equipamento
    Então devo ver o texto:     ${saida}


# Erro na criação de um equipo
# Ocorreu um erro na criação de um equipo, tente novamente mais tarde!

# Data de Entrega: Dia 8 de Setembro de 2020 até as 10h
# Entrar nos comentários da Video Aulas e no email fernando@qaninja.com.br / cc oi@qaninja.com.br

# Premiação: Os cinco primeiros que entregaram ganhando uma vaga no DevTester!
# 5 Vagas!!!!!